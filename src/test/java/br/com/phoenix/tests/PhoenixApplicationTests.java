package br.com.phoenix.tests;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.phoenix.PhoenixApplication;
import br.com.phoenix.processor.ProcessorFileBusiness;
import br.com.phoenix.utils.FileManager;

@ExtendWith(MockitoExtension.class)
class PhoenixApplicationTests {

	@InjectMocks private ProcessorFileBusiness processorFileBusiness;

	@Mock private FileManager fileManager;

	@Test
	void contextLoads() {
		PhoenixApplication.main(new String[] {});
		Assertions.assertTrue(true);
	}

	@Test
	void fileLoadNullPointerException() {
		NullPointerException exception = Assertions.assertThrows(NullPointerException.class, () -> {
			processorFileBusiness.loadFile(StringUtils.EMPTY);
		});
		Assertions.assertNull(exception.getMessage());
		Assertions.assertEquals(null, exception.getMessage());
	}

}
