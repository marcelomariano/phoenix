package br.com.phoenix.services;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import br.com.phoenix.processor.ProcessorFileBusiness;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@Profile("local")
@RequiredArgsConstructor
public class WatcherService {

	@Value("${sourceFolder}")
	private String sourceFolder;

	private final ProcessorFileBusiness proccessFile;

	@Bean
	@Profile("local")
	@SneakyThrows
	public boolean watch() {
		log.info("start watch folder {}", sourceFolder);
		Path path = getSourcePath();
		WatchService watchService = FileSystems.getDefault().newWatchService();
		path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
		watch(watchService);
		return true;
	}

	private Path getSourcePath() {
		return Paths.get(sourceFolder);
	}

	public boolean watch(WatchService watchService) throws InterruptedException {
		WatchKey key;
		while ((key = watchService.take()) != null) {
			key.pollEvents().stream().forEach(event -> {
				String fileName = event.context().toString();
				log.info("Event called: {} on file: {}.", event.kind(), fileName);
				proccessFile.proccessFile(fileName);
			});
			key.reset();
		}
		return true;
	}
}