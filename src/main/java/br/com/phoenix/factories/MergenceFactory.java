package br.com.phoenix.factories;

import br.com.phoenix.constant.Messages;
import br.com.phoenix.exceptions.BusinessException;
import br.com.phoenix.model.Client;
import br.com.phoenix.model.Mergin;
import br.com.phoenix.model.Sale;
import br.com.phoenix.model.Seller;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MergenceFactory {

	public static Mergin decode(String[] data) throws BusinessException {
		switch (data[0]) {
		case "001":
			return new Seller(data);
		case "002":
			return new Client(data);
		case "003":
			return new Sale(data);
		default:
			 throw new  BusinessException(Messages.INVALID_DATA);
		}
	}
}
