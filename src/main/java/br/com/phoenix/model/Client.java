package br.com.phoenix.model;

import static br.com.phoenix.constant.Messages.INVALID_DATA;

import br.com.phoenix.constant.Messages;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.experimental.Wither;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
@Wither
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Client extends Mergin {

	private static final String TYPE = "002";

	private String cnpj;
	private String name;
	private String businessArea;

	@SneakyThrows
	public Client(String[] args) {
		setType(TYPE);
		if (!isValid(args)) {
			log.warn(INVALID_DATA);
			Messages.validationException();
		}
		withCnpj(args[1]).withName(args[2]).withBusinessArea(args[3]);
	}

	protected boolean isValid(String[] args) {
		return args.length == 4 && getType().equals(args[0]);
	}

}