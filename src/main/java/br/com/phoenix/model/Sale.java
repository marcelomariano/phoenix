package br.com.phoenix.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.com.phoenix.constant.Messages;
import br.com.phoenix.exceptions.BusinessException;
import br.com.phoenix.utils.EncodeDecodeUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import lombok.experimental.Wither;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Wither
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Sale extends Mergin {

	private static final String TYPE = "003";

	private String 		saleId;
	private String 		salesman;
	private List<Item> 	items;
	private BigDecimal 	totalSale = new BigDecimal(0);

	@SneakyThrows
	public Sale(String[] args) throws BusinessException {
		setType(TYPE);
		Optional.of(isValid(args)).filter(Boolean::booleanValue).ifPresent(valid -> {
			if (!valid) {
				Messages.validationException();
			}
		});
		withSaleId(args[1]).withItems(EncodeDecodeUtils.decodeList(args[2], totalSale)).withSalesman(args[3]);
	}

	protected boolean isValid(String[] args) {
		return args.length == 4 && getType().equals(args[0]);
	}

}