package br.com.phoenix.constant;

import br.com.phoenix.exceptions.BusinessException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Messages {

	public static final String INVALID_DATA = "invalid data.";

	@SneakyThrows
	public static void validationException() {
		log.warn(INVALID_DATA);
		throw new BusinessException(INVALID_DATA);
	}

}
