package br.com.phoenix.constant;

public class RegexConstants {

	public static final String DECODE = "(\\\\[|\\\\])";
	public static final String SPLITER = "(?=ç[0-9A-Z\\[])ç";
	public static final String REGEX_FILE_NAME = "(.*)\\\\.dat$";
}
